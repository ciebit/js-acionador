import Acionador from './Acionador';

class Inativador
{
    private Acionador: Acionador;
    private static contador: number = 0;
    private Elemento: HTMLElement;
    private eventosObservados: Object;
    private id: number;
    private ignorarAlvo: boolean;
    private situacaoLigado: boolean;

    public constructor(elemento:HTMLElement, acionador:Acionador, eventoPadrao:boolean = true)
    {
        this.Acionador = acionador;
        this.Elemento = elemento;
        this.eventosObservados = {};
        this.id = Inativador.contador;
        this.ignorarAlvo = true;

        if (eventoPadrao) {
            this.adicionarEvento("click", false);
        }

        Inativador.contador ++;
    }

    public adicionarEvento(evento:string, disparoPadrao:boolean = false): this
    {
        this.eventosObservados[evento] = disparoPadrao;
        this.Elemento.addEventListener(evento, this.eventoPadrao.bind(this));
        return this;
    }

    public desligar(): this
    {
        this.Acionador.desligarEventos();
        return this;
    }

    private eventoPadrao(Evento:Event): void
    {
        if (this.ignorarAlvo && this.Acionador.domIgual(<Element> Evento.target)) {
            return;
        }

        let disparoPadrao:boolean = this.eventosObservados[Evento.type];

        if (disparoPadrao) {
            Evento.preventDefault();
        }

        this.inativar();
    }

    public inativar(): this
    {
        this.Acionador.inativar();
        return this;
    }

    public ligar(): this
    {
        this.Acionador.ligarEventos();
        return this;
    }

    public obterId(): number
    {
        return this.id;
    }

    public obterSituacaoLigado(): boolean
    {
        return this.situacaoLigado;
    }

    public removerEvento(evento:string): this
    {
        this.Elemento.removeEventListener(evento, this.eventoPadrao.bind(this));
        return this;
    }
}

export default Inativador;
