import Hermes from '@ciebit/hermes';

class Alvo
{
    private classeAtivo: string;
    private classeInativo: string;
    private static contador: number = 0;
    private Elemento: HTMLElement;
    private Hermes: Hermes;
    private id: number;

    public constructor(Elemento:HTMLElement)
    {
        this.id = Alvo.contador;
        this.Elemento = Elemento;
        this.Hermes = new Hermes;

        this.classeAtivo = 'cbjsAcionador__alvo--ativo';
        this.classeInativo = 'cbjsAcionador__alvo--inativo';

        if (! this.Elemento.classList.contains(this.classeAtivo)) {
            this.inativar();
        }

        Alvo.contador ++;
    }

    public aviseMe(evento, funcao):this
    {
        this.Hermes.aviseMe(evento, funcao)
        return this;
    }

    public ativar(): this
    {
        this.Elemento.classList.remove(this.classeInativo);
        this.Elemento.classList.add(this.classeAtivo);

        this.Hermes.avise("ativado")

        return this;
    }

    public definirClasseAtivo(classe: string): this
    {
        this.classeAtivo = classe;
        return this;
    }

    public definirClasseInativo(classe: string): this
    {
        this.classeInativo = classe;
        return this;
    }

    public inativar(): this
    {
        this.Elemento.classList.remove(this.classeAtivo);
        this.Elemento.classList.add(this.classeInativo);

        this.Hermes.avise("inativado");

        return this;
    }

    public obterId(): number
    {
        return this.id;
    }
}

export default Alvo;
