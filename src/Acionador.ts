import Hermes from '@ciebit/hermes';
import AlvoObj from './Alvo';

class Acionador
{
    private Alvo: AlvoObj;
    private classeAtivo: string;
    private classeInativo: string;
    private static contador: number = 0;
    private Elemento: HTMLElement;
    private Hermes: Hermes;
    private eventosComportamentos: Object;
    private id: number;

    public constructor(Elemento:HTMLElement, alvo:AlvoObj, eventoPadrao:boolean = true)
    {
        this.id = Acionador.contador;
        this.Hermes = new Hermes;

        this.Alvo = alvo;
        this.classeAtivo = 'cbjsAcionador--ativo';
        this.classeInativo = 'cbjsAcionador--inativo';
        this.Elemento = Elemento;
        this.eventosComportamentos = {};

        if (eventoPadrao) {
            this.adicionarEvento('click', false);
        }

        if (! this.Elemento.classList.contains(this.classeAtivo)) {
            this.inativar();
        }

        Acionador.contador ++;
    }

    public adicionarEvento(evento:string, disparoPadrao:boolean = false): this
    {
        this.eventosComportamentos[evento] = disparoPadrao;
        this.Elemento.addEventListener(evento, this.eventoPadrao.bind(this));

        return this;
    }

    public alternar(): this
    {
        this.obterSituacao() ? this.inativar() : this.ativar();

        return this;
    }

    public desligarEventos(): this
    {
        let objetos:Array<string> = Object.keys(this.eventosComportamentos);
        objetos.forEach((evento:string) => { this.revemoverEvento(evento); });
        return this;
    }

    public domIgual(elemento:Element): boolean
    {
        return elemento === this.Elemento;
    }

    private eventoPadrao(Evento:Event)
    {
        let disparoPadrao:boolean = this.eventosComportamentos[Evento.type];

        if (! disparoPadrao) {
            Evento.preventDefault();
        }

        this.alternar();
    }

    public aviseMe(evento, funcao):this
    {
        this.Hermes.aviseMe(evento, funcao)
        return this;
    }

    public ativar(): this
    {
        this.Elemento.classList.remove(this.classeInativo);
        this.Elemento.classList.add(this.classeAtivo);


        this.Alvo.ativar();
        this.Hermes.avise("ativado", this);

        return this;
    }

    public definirClasseAtivo(classe: string): this
    {
        this.classeAtivo = classe;
        return this;
    }

    public definirClasseInativo(classe: string): this
    {
        this.classeInativo = classe;
        return this;
    }

    public inativar(): this
    {
        this.Elemento.classList.remove(this.classeAtivo);
        this.Elemento.classList.add(this.classeInativo);

        this.Alvo.inativar();
        this.Hermes.avise("inativado");

        return this;
    }

    public ligarEventos(): this
    {
        let objetos:Array<string> = Object.keys(this.eventosComportamentos);
        objetos.forEach((evento:string) => {
            this.adicionarEvento(evento, this.eventosComportamentos[evento]);
        });

        return this;
    }

    public obterAlvo(): AlvoObj
    {
        return this.Alvo;
    }

    public obterId(): number
    {
        return this.id;
    }

    public obterSituacao(): boolean
    {
        return this.Elemento.classList.contains(this.classeAtivo);
    }

    public revemoverEvento(evento:string): this
    {
        this.Elemento.removeEventListener(evento, this.eventoPadrao.bind(this));
        return this;
    }
}

export default Acionador;
