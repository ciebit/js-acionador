import Acionador from './../Acionador';
import Alvo from './../Alvo';
import Hermes from '@ciebit/hermes';
import Inativador from './../Inativador';

class AutoExecuta
{
    private static acionadores: Array<Acionador>;
    private static construido: boolean;
    private static grupos: Object;
    private static inativadores: Array<Inativador>;
    private static seletorAcionadores: string;
    private Hermes: Hermes;

    public constructor(document: Document)
    {
        if (AutoExecuta.construido) {
            return;
        }

        AutoExecuta.grupos = {};

        AutoExecuta.acionadores = [];
        AutoExecuta.inativadores = [];
        AutoExecuta.seletorAcionadores = '.cbjsAcionador';

        document.addEventListener('DOMContentLoaded', () => this.ligar());

        AutoExecuta.construido = true;

        return this;
    }

    private inativarElementosGrupo(acionador):this
    {
        let grupo = acionador.Elemento.getAttribute("data-grupo");
        for (let i = 0; i < AutoExecuta.grupos[grupo].length; i++) {
            if (AutoExecuta.grupos[grupo][i].obterId() == acionador.obterId()) {
                continue;
            }
            AutoExecuta.grupos[grupo][i].inativar();
        }
        return this;
    }

    private ligar(): AutoExecuta
    {
        let acionadoresElementos = <NodeListOf<HTMLElement>>document.querySelectorAll(AutoExecuta.seletorAcionadores);

        //Adicionando as chaves do Objeto grupos
        for (let i = 0; i < acionadoresElementos.length; i++) {
            let grupo = acionadoresElementos[i].getAttribute("data-grupo");

            AutoExecuta.grupos[grupo] = [];
        }
        let listaGrupos = Object.keys(AutoExecuta.grupos)

        let x = 0;
        for (let i = 0; i < acionadoresElementos.length; i++) {
            let AlvoElemento = <HTMLElement>document.querySelector(acionadoresElementos[i].getAttribute('data-alvo'));
            let InativadorElemento = <HTMLElement>document.querySelector(acionadoresElementos[i].getAttribute('data-inativador'));

            if (! AlvoElemento) {
                return this;
            }

            let AlvoObj = new Alvo(AlvoElemento);
            let AcionadorObj = new Acionador(acionadoresElementos[i], AlvoObj);
            AutoExecuta.acionadores.push(AcionadorObj);

            if (InativadorElemento) {
                let InativadorObj = new Inativador(InativadorElemento, AcionadorObj);
                AutoExecuta.inativadores.push(InativadorObj);
            }

            // Alimentando as chaves do Objeto grupos
            let grupoAtributo = acionadoresElementos[i].getAttribute("data-grupo");
            for (let x = 0; x < listaGrupos.length; x++) {
                if (grupoAtributo == listaGrupos[x]) {
                    AutoExecuta.grupos[listaGrupos[x]].push(Acionador);
                }
            }

            AcionadorObj.aviseMe('ativado', this.inativarElementosGrupo);
        }

        return this;
    }
}

export default AutoExecuta;
