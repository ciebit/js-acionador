# JS-Acionador

Essa ferramenta foi criada com o objetivo de simplificar o processo de acionamento de um elemento do DOM, ela poderá ser usada para várias finalidades mas consiste em exibir/ocultar, aumentar/reduzir e etc.

O pacote é composto por três elementos principais, são eles:

* Alvo
* Acionador
* Inativador

Obtenha mais informações através da nossa [Wiki](https://bitbucket.org/ciebit/js-acionador/Home)

Issues podem ser abertas em https://bitbucket.org/ciebit/js-acionador/issues
