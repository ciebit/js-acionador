requirejs.config({
    paths: {
        "@ciebit/hermes": "http://js.ciebit.com/hermes/v3.0.3"
    }
});
define("Alvo", ["require", "exports", "@ciebit/hermes"], function (require, exports, hermes_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Alvo = (function () {
        function Alvo(Elemento) {
            this.id = Alvo.contador;
            this.Elemento = Elemento;
            this.Hermes = new hermes_1.default;
            this.classeAtivo = 'cbjsAcionador__alvo--ativo';
            this.classeInativo = 'cbjsAcionador__alvo--inativo';
            if (!this.Elemento.classList.contains(this.classeAtivo)) {
                this.inativar();
            }
            Alvo.contador++;
        }
        Alvo.prototype.aviseMe = function (evento, funcao) {
            this.Hermes.aviseMe(evento, funcao);
            return this;
        };
        Alvo.prototype.ativar = function () {
            this.Elemento.classList.remove(this.classeInativo);
            this.Elemento.classList.add(this.classeAtivo);
            this.Hermes.avise("ativado");
            return this;
        };
        Alvo.prototype.definirClasseAtivo = function (classe) {
            this.classeAtivo = classe;
            return this;
        };
        Alvo.prototype.definirClasseInativo = function (classe) {
            this.classeInativo = classe;
            return this;
        };
        Alvo.prototype.inativar = function () {
            this.Elemento.classList.remove(this.classeAtivo);
            this.Elemento.classList.add(this.classeInativo);
            this.Hermes.avise("inativado");
            return this;
        };
        Alvo.prototype.obterId = function () {
            return this.id;
        };
        Alvo.contador = 0;
        return Alvo;
    }());
    exports.default = Alvo;
});
define("Acionador", ["require", "exports", "@ciebit/hermes"], function (require, exports, hermes_2) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Acionador = (function () {
        function Acionador(Elemento, alvo, eventoPadrao) {
            if (eventoPadrao === void 0) { eventoPadrao = true; }
            this.id = Acionador.contador;
            this.Hermes = new hermes_2.default;
            this.Alvo = alvo;
            this.classeAtivo = 'cbjsAcionador--ativo';
            this.classeInativo = 'cbjsAcionador--inativo';
            this.Elemento = Elemento;
            this.eventosComportamentos = {};
            if (eventoPadrao) {
                this.adicionarEvento('click', false);
            }
            if (!this.Elemento.classList.contains(this.classeAtivo)) {
                this.inativar();
            }
            Acionador.contador++;
        }
        Acionador.prototype.adicionarEvento = function (evento, disparoPadrao) {
            if (disparoPadrao === void 0) { disparoPadrao = false; }
            this.eventosComportamentos[evento] = disparoPadrao;
            this.Elemento.addEventListener(evento, this.eventoPadrao.bind(this));
            return this;
        };
        Acionador.prototype.alternar = function () {
            this.obterSituacao() ? this.inativar() : this.ativar();
            return this;
        };
        Acionador.prototype.desligarEventos = function () {
            var _this = this;
            var objetos = Object.keys(this.eventosComportamentos);
            objetos.forEach(function (evento) { _this.revemoverEvento(evento); });
            return this;
        };
        Acionador.prototype.domIgual = function (elemento) {
            return elemento === this.Elemento;
        };
        Acionador.prototype.eventoPadrao = function (Evento) {
            var disparoPadrao = this.eventosComportamentos[Evento.type];
            if (!disparoPadrao) {
                Evento.preventDefault();
            }
            this.alternar();
        };
        Acionador.prototype.aviseMe = function (evento, funcao) {
            this.Hermes.aviseMe(evento, funcao);
            return this;
        };
        Acionador.prototype.ativar = function () {
            this.Elemento.classList.remove(this.classeInativo);
            this.Elemento.classList.add(this.classeAtivo);
            this.Alvo.ativar();
            this.Hermes.avise("ativado", this);
            return this;
        };
        Acionador.prototype.definirClasseAtivo = function (classe) {
            this.classeAtivo = classe;
            return this;
        };
        Acionador.prototype.definirClasseInativo = function (classe) {
            this.classeInativo = classe;
            return this;
        };
        Acionador.prototype.inativar = function () {
            this.Elemento.classList.remove(this.classeAtivo);
            this.Elemento.classList.add(this.classeInativo);
            this.Alvo.inativar();
            this.Hermes.avise("inativado");
            return this;
        };
        Acionador.prototype.ligarEventos = function () {
            var _this = this;
            var objetos = Object.keys(this.eventosComportamentos);
            objetos.forEach(function (evento) {
                _this.adicionarEvento(evento, _this.eventosComportamentos[evento]);
            });
            return this;
        };
        Acionador.prototype.obterAlvo = function () {
            return this.Alvo;
        };
        Acionador.prototype.obterId = function () {
            return this.id;
        };
        Acionador.prototype.obterSituacao = function () {
            return this.Elemento.classList.contains(this.classeAtivo);
        };
        Acionador.prototype.revemoverEvento = function (evento) {
            this.Elemento.removeEventListener(evento, this.eventoPadrao.bind(this));
            return this;
        };
        Acionador.contador = 0;
        return Acionador;
    }());
    exports.default = Acionador;
});
define("Inativador", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Inativador = (function () {
        function Inativador(elemento, acionador, eventoPadrao) {
            if (eventoPadrao === void 0) { eventoPadrao = true; }
            this.Acionador = acionador;
            this.Elemento = elemento;
            this.eventosObservados = {};
            this.id = Inativador.contador;
            this.ignorarAlvo = true;
            if (eventoPadrao) {
                this.adicionarEvento("click", false);
            }
            Inativador.contador++;
        }
        Inativador.prototype.adicionarEvento = function (evento, disparoPadrao) {
            if (disparoPadrao === void 0) { disparoPadrao = false; }
            this.eventosObservados[evento] = disparoPadrao;
            this.Elemento.addEventListener(evento, this.eventoPadrao.bind(this));
            return this;
        };
        Inativador.prototype.desligar = function () {
            this.Acionador.desligarEventos();
            return this;
        };
        Inativador.prototype.eventoPadrao = function (Evento) {
            if (this.ignorarAlvo && this.Acionador.domIgual(Evento.target)) {
                return;
            }
            var disparoPadrao = this.eventosObservados[Evento.type];
            if (disparoPadrao) {
                Evento.preventDefault();
            }
            this.inativar();
        };
        Inativador.prototype.inativar = function () {
            this.Acionador.inativar();
            return this;
        };
        Inativador.prototype.ligar = function () {
            this.Acionador.ligarEventos();
            return this;
        };
        Inativador.prototype.obterId = function () {
            return this.id;
        };
        Inativador.prototype.obterSituacaoLigado = function () {
            return this.situacaoLigado;
        };
        Inativador.prototype.removerEvento = function (evento) {
            this.Elemento.removeEventListener(evento, this.eventoPadrao.bind(this));
            return this;
        };
        Inativador.contador = 0;
        return Inativador;
    }());
    exports.default = Inativador;
});
define("Controladores/AutoExecuta", ["require", "exports", "Acionador", "Alvo", "Inativador"], function (require, exports, Acionador_1, Alvo_1, Inativador_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var AutoExecuta = (function () {
        function AutoExecuta(document) {
            var _this = this;
            if (AutoExecuta.construido) {
                return;
            }
            AutoExecuta.grupos = {};
            AutoExecuta.acionadores = [];
            AutoExecuta.inativadores = [];
            AutoExecuta.seletorAcionadores = '.cbjsAcionador';
            document.addEventListener('DOMContentLoaded', function () { return _this.ligar(); });
            AutoExecuta.construido = true;
            return this;
        }
        AutoExecuta.prototype.inativarElementosGrupo = function (acionador) {
            var grupo = acionador.Elemento.getAttribute("data-grupo");
            for (var i = 0; i < AutoExecuta.grupos[grupo].length; i++) {
                if (AutoExecuta.grupos[grupo][i].obterId() == acionador.obterId()) {
                    continue;
                }
                AutoExecuta.grupos[grupo][i].inativar();
            }
            return this;
        };
        AutoExecuta.prototype.ligar = function () {
            var acionadoresElementos = document.querySelectorAll(AutoExecuta.seletorAcionadores);
            for (var i = 0; i < acionadoresElementos.length; i++) {
                var grupo = acionadoresElementos[i].getAttribute("data-grupo");
                AutoExecuta.grupos[grupo] = [];
            }
            var listaGrupos = Object.keys(AutoExecuta.grupos);
            var x = 0;
            for (var i = 0; i < acionadoresElementos.length; i++) {
                var AlvoElemento = document.querySelector(acionadoresElementos[i].getAttribute('data-alvo'));
                var InativadorElemento = document.querySelector(acionadoresElementos[i].getAttribute('data-inativador'));
                if (!AlvoElemento) {
                    return this;
                }
                var AlvoObj = new Alvo_1.default(AlvoElemento);
                var AcionadorObj = new Acionador_1.default(acionadoresElementos[i], AlvoObj);
                AutoExecuta.acionadores.push(AcionadorObj);
                if (InativadorElemento) {
                    var InativadorObj = new Inativador_1.default(InativadorElemento, AcionadorObj);
                    AutoExecuta.inativadores.push(InativadorObj);
                }
                var grupoAtributo = acionadoresElementos[i].getAttribute("data-grupo");
                for (var x_1 = 0; x_1 < listaGrupos.length; x_1++) {
                    if (grupoAtributo == listaGrupos[x_1]) {
                        AutoExecuta.grupos[listaGrupos[x_1]].push(Acionador_1.default);
                    }
                }
                AcionadorObj.aviseMe('ativado', this.inativarElementosGrupo);
            }
            return this;
        };
        return AutoExecuta;
    }());
    exports.default = AutoExecuta;
});
define("index", ["require", "exports", "Acionador", "Alvo", "Inativador", "Controladores/AutoExecuta"], function (require, exports, Acionador_2, Alvo_2, Inativador_2, AutoExecuta_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.Acionador = Acionador_2.default;
    exports.Alvo = Alvo_2.default;
    exports.Inativador = Inativador_2.default;
    exports.AutoExecuta = AutoExecuta_1.default;
});
define("@ciebit/acionador", ['index'], function (index) {
    "use strict";
    return index;
});
